---
title: "Institución"
subtitle: ""
# meta description
description: "This is meta description"
draft: false
---

{{< figure src="/images/institucion/edificio.jpg" title="Foto de nuestro edificio" >}}

Nuestra institución nació hace muchos años, un 6 octubre de 1957, cuando un grupo de personas que amaban la música y el canto se reunieron y formaron el Coro Estable de la Escuela de Canto Coral. Al principio, y durante muchos años, sin una sede propia; deambulando por muchas instituciones que nos cobijaban y prestaban un lugarcito para nuestros ensayos.

Por nuestras cuerdas pasaron muchos coreutas, algunos ahora son directores de otros coros o cantantes de renombre, y aún conservamos con nosotros algunos de aquellos primeros entusiastas que nos dieron origen. Desde 1988 contamos con un coro de niños, donde se forman voces educadas para el canto desde edad temprana. Luego, como los chicos crecen, surgió la necesidad de crear un coro juvenil; desde el cual, al llegar a los 18 años, los coreutas pasan al coro estable. Tanto el coro Estable como el coro de Niños son dirigidos por Marcela Castiglioni. éste último dejó de funcionar en el año 2008.

El 24 de abril de 2001 se creó el Taller de Formación coral, de donde los coros de la institución toman sus voces, con la nivelación necesaria para un eficiente desempeño. Está dirigido por Mary Morales.

En el año 2002 comenzó su trabajo el Taller de Formación vocal, bajo la conducción de la fonoaudióloga Graciela Dondero, este taller finalizó sus actividades dos años después.

El año 2003 vio nacer, bajo la dirección del Maestro J. C. Benavídez, la Orquesta de Vientos. de la Escuela, esta orquesta finalizó luego sus actividades por ocupaciones del maestro director.

La dirección de la institución está en manos de una Comisión Directiva formada por coreutas, según las normas dictadas por sus estatutos.