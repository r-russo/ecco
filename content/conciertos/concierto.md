---
title: "Concierto de ejemplo"
date: 2020-04-18T10:07:21
image: "/images/conciertos/prueba.jpg"
type: "featured"
description: "This is meta description"
draft: false
---

Realizado en tal lugar a tal hora por tal motivo

## Repertorio

* Obra 1
* Obra 2
* Etc.

## Videos

{{< youtube FbtQMJ3bFA0 >}}
