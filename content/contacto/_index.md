---
title: "Contactanos"
subtitle: ""
# meta description
description: "This is meta description"
draft: false
---


* **Teléfono: +54 9 223 492-3852** 
* **Mail: eccomdp@yahoo.com.ar**
* **Dirección: Catamarca 1065, Mar del Plata, Buenos Aires. Argentina**

<iframe width="80%" height="150" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.openstreetmap.org/export/embed.html?bbox=-57.549543678760536%2C-37.99264325126362%2C-57.54600316286088%2C-37.990675256992965&amp;layer=mapnik&amp;marker=-37.99165926072784%2C-57.5477734208107"></iframe><br/>

## Sumate 
Sé parte del coro blabla

## Contrataciones
Podemos cantar en la reunión que has preparado con tanto esmero y dedicación, nos presentamos con un atractivo y sobrio atuendo.

Vení a escucharnos para elegir el repertorio que más se adapte a tu necesidad.

### Servicios
* Conciertos en apertura y cierre de Convenciones
* Casamientos
* Fiestas 
