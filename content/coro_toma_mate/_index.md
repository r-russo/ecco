---
title: "Coro Tomá Mate"
subtitle: ""
# meta description
description: "This is meta description"
draft: false
---

## Avisos importantes

* Ensayos suspendidos por el momento

## Horario de ensayo

*Sábados de 16 a 18hs*

## Partituras

* [Link a Google Drive](https://drive.google.com/drive/folders/0B6xl7YDPe3qZfkdHNHRfcXVySmxjUEJjQzRnbTA1cmNUa1dOQlZHbm1Qak1uVUdDcFcxcm8?usp=sharing)

## Historia