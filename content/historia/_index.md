---
title: "Historia"
subtitle: ""
# meta description
description: "This is meta description"
draft: false
---

# Comienzos

{{< figure src="/images/historia/primeros_integrantes.jpg" title="Primeros integrantes" >}}

En marzo de 1956, un grupo de amantes de la música y el canto, desprendiéndose del **Grupo de Teatro ABC**,
solicitó incorporación de voces y publicó en los diarios la iniciación de las actividades de un nuevo coro,
quizás el primer coro local, con representación genuina de la ciudad. Con la colaboración del maestro **Luis
Aníbal Paganotto**, inició sus ensayos el 26 de mayo de 1956.

Con aspiración a conformar la nueva entidad como Escuela, se impartían clases de Solfeo, Historia de la
Música y otros temas inherentes a la actividad coral. De a poco el Coro fue consolidándose y brindando
conciertos. En cuanto a la incorporación de voces, la mayoría de los que se presentaban carecían de
experiencia coral, lo que se suplía con entusiasmo y deseos de aprender. No era sencillo atraer público a los
conciertos, no obstante se continuó adelante, siempre con la tutoría del maestro Luis Aníbal Paganotto, que
conducía la entidad no sólo en el aspecto musical sino también en la organización de los conciertos.

En el primer concierto de que tenemos registro, el día sábado 19 de Enero de 1957 participaron como
integrantes:

Abad Ángeles, Abad Águeda, Ambos Ernesto, Blanco Dolores, Bosatta Julio, Capurro Federico, Cremonte
Nélida, Dellater Frank, Dell’Orso Juan Carlos, Franzese Elena, Guarnieri Concepción, Maffia Isabel, Manzano
Noemí, Maljasian Irma, Ortiz Rolando, Olson Eduardo, Salinas Mireya, Valentín Mario y Verburk Ángela.

{{< figure src="/images/historia/primer_programa_anverso.jpg" title="Anverso del programa del primer concierto" >}}

{{< figure src="/images/historia/primer_programa_reverso.jpg" title="Reverso del programa del primer concierto" >}}

En esos primeros tiempos fue fundamental el enorme apoyo que brindó el maestro **Manuel Rego**,
reconocido intérprete en el país y promotor de la creación del Coro de la Escuela y del Conservatorio
Provincial.

A mediados de 1957 se tomó la decisión de comprar un órgano electrónico para acompañar al Coro en sus
presentaciones. El órgano, de la firma argentina Guazzotti, era para esa época un instrumento de mucha
importancia desde los puntos de vista artístico y técnico, Para su inauguración, se había visto la posibilidad
de que lo estrenara el maestro **Héctor Zeoli**, organista, quien posteriormente fuera director de la institución.

## Nacimiento de la Institución

El 30 de Agosto de 1957 se produjo el nacimiento de la escuela bajo la denominación de Escuela de Canto
Coral de Mar del Plata (ECCo). Los coreutas, sus *alumnos*, decidieron en asamblea crear una Asociación
Civil sin fines de lucro, lo que se ratificó en la Actuación Notarial del 6 de Octubre, fecha tomada como
fundacional.

En los estatutos de la nueva asociación civil, quedaban fijadas las siguientes premisas:

* La promoción y el conocimiento de la música y el canto coral, mediante el auspicio y la organización de diversos conciertos y encuentros
* El desarrollo de un Coro Estable propio, al objeto de que Mar del Plata pudiera aportar voces a las orquestas, conjuntos, solistas y espectáculos que visitan la ciudad, siendo éste el principal propósito que mueve desde entonces a los dirigentes y asociados de la Escuela.
* La creación de una Escuela para la preparación de voces corales con el fin de fomentar la promoción de otros coros.

A los citados efectos, la ya constituida ECCo designó autoridades y una Comisión de Estatuto y, una vez
obtenida su personería jurídica, fue reconocida por la Municipalidad como **Entidad de Bien Público N° 65**.

La ECCo es, por tanto, una Institución popular dedicada a enriquecer y promover la cultura del pueblo
marplatense. Desde sus inicios optó por desarrollar y perfeccionar su propio Coro Estable y, más adelante,
por la mediación y el esfuerzo de su propia dirigencia, y ante la inexistencia de otras instituciones locales
que lo hicieran, tomó la posta en la realización de conciertos superando las dificultades derivadas de la falta
de salas o de montajes adecuados para la presentación de espectáculos, gracias a la imaginación y esfuerzo
de sus dirigentes y asociados.

Ello posibilitó que, mediante costos accesibles, el pueblo de Mar del Plata pudiera deleitarse con la
actuación de destacadísimas figuras de la música nacional e internacional, conciertos, megaconciertos y
obras en primera o segunda presentación.

En el interín, e incluso careciendo de sede propia, la ECCo continuó desarrollando su Coro Estable y su ya
citado objetivo de aportar voces a otras formaciones y espectáculos, convirtiéndose en Escuela de coreutas
y coros, brindando educación y preparación coral, así como infinidad de conciertos, muchos de ellos a
solicitud de la Provincia o del Municipio, otros a beneficio de entidades de bien público.

# Los años siguen pasando

Pasan los años, se suceden más conciertos, viajes, encuentros, algunos coreutas se van, parte del ciclo
ininterrumpido de la vida, otros llegan a incorporarse a la gran familia que es la Escuela.
El 10 de Junio de 1986 asume la dirección artística de la Institución nuestra actual directora, **Marcela María
Castiglioni**, y la profesora **Mary Morales** es la asistente de dirección.

## Coro de niños y jóvenes

{{< figure src="/images/historia/coro_ninos.jpg" title="Coro de Niños" >}}

Cultivar en los niños el gusto por la música es el anhelo de nuestra Institución. El Coro de Niños nació de la
necesidad de educar voces para el canto desde edad temprana, para que los más chiquitos tuvieran contacto
con el arte a través del canto coral. El Coro **Voces de Primavera** se creó en 1988, por medio de un convenio
entre la Escuela de Canto Coral y la Municipalidad de General Pueyrredón, la cual, con su aporte hizo posible
que la ciudad tuviese un coro de niños gratuito, cuya organización y gestión se encomendó a la Escuela,
celebrándose los ensayos en la misma Biblioteca.

En 1990, habiendo crecido ya una gran parte de los integrantes del Coro de Niños, y necesitando éstos mayores
desafíos, tanto en responsabilidades como en repertorio, se creó el **Coro Juvenil**, formado a cuatro voces, con
edades comprendidas entre los 14 y los 18 años. En 1992 el Coro Juvenil ganó el Distrital de los Torneos Deportivos y Culturales Bonaerenses.

{{< figure src="/images/historia/coro_jovenes.jpg" title="Coro Juvenil" >}}

## Dos viajes soñados

En Julio de 1996 la Escuela viajó a Galicia, invitada por el **Gobierno Gallego**, y fue declarada Embajadora
Cultural de Mar del Plata, por la Municipalidad de General Pueyrredón. Así el coro Estable realizó una
gira de 15 días por España, ofreciendo conciertos en varias ciudades de Galicia, como Cambados, Lalín,
Ponteareas, Villagarcía de Arousa y A Estrada, participando también en la Misa del Peregrino, en la Catedral
de Santiago de Compostela.

En Julio de 1998 la ECCo fue invitada oficialmente por **El Vaticano** para cantar en la **Basílica de San Pedro**<,
realizando una gira por El Vaticano, Italia, Francia y España, nuevamente declarada Embajadora Cultural
de Mar del Plata. El 19 de Julio de 1998, a la 8 h., debíamos estar en la Basílica de San Pedro para cantar en
la Misa Principal de las 11 hs., multitudinaria y concelebrada por 25 obispos de distintas nacionalidades, y
uno de los motivos que dieron origen al extraordinario viaje realizado.

## En la Audiencia Papal

Una de las actividades más emotivas en el Vaticano fue nuestra entrevista con **Su
Santidad**, en la Audiencia de los miércoles. Estaba prevista para las 10 hs., y antes del
ingreso había que pasar por las medidas de seguridad y las acreditaciones. Finalmente
la Escuela entró en un salón de medidas impresionantes, donde nos ubicamos en el
lugar que nos asignaron; y habiendo pasado unos minutos, apareció el Santo Padre
caminando muy despacio por el inmenso escenario. Allí nos mencionó en su alocución,
en castellano, pues se dirigió a cada grupo en su idioma. Cuando nos correspondió,
cantamos *a capella* y con un nudo en la garganta, motivado por la emoción del
momento, el Himno a la Alegría.

También hemos disfrutado de muchos viajes más *caseros*, a Buenos Aires en varias
oportunidades, Tucumán, Córdoba, Tandil, Castelli, Maipú, González Chávez, De la
Garma, Junín, Pringles, Coronel Suárez, Santa Fé, Esperanza, Necochea, Miramar, Villa
Gesell.

## Logro de la sede propia

A lo largo de toda su historia, la Escuela ha peregrinado por diferentes lugares de la ciudad para poder efectuar
sus ensayos. A continuación figura un listado parcial de los mismos:

* Centro Gallego de Mar del Plata
* LU6 Radio Atlántica
* Escuela Número 6
* Centro Navarro del Sur
* Alianza Francesa
* Colegio Day School
* Club Pueyrredón
* Colegio de Ingenieros
* Ex Colegio Sarmiento
* Bromatología del Municipio (Edificio abandonado que hoy es nuestra sede)
* La Cueva de la Sirena (Boite en subsuelo al lado de la actual sede, en aquel momento sin actividad)
* Hogar de la Familia de Nelly y Rodolfo Eandi
* Edificio policial Juan Vucetich
* Cooperativa Farmacéutica FARMA
* Casa de María Ana Pasolini
* Colegio Jesús Redentor
* Biblioteca Pública Municipal
* Centro Vasco
* Iglesia Nueva Pompeya
* Sede del Partido Socialista

Como primer paso para lograr este objetivo, la **Comisión Directiva** actuante en el año 1993, representada por su
Presidente, Sr. **Crisanto Cuerdo**, su Vicepresidente Sr. **Ramón Barbero**, y el Sr. **Aldo Barba** como asesor, iniciaron
averiguaciones y trámites para conseguir del Municipio la donación de una propiedad para sede de la Escuela.

Tras tres años de trámites, entrevistas, idas y vueltas, esperas; con fecha 24 de Febrero de 1995, en la Ordenanza
con registro No. 0-4056, el Presidente del Honorable Concejo Deliberante, Sr. Fernando Álvarez, insistió en la
sanción de la **Ordenanza No. 0-4043** del 16 Enero de 1995, por la que se otorgaba en Comodato a la Escuela de
Canto Coral el inmueble de la calle Catamarca 1065, que en un tiempo había sido sede de Bromatología de la
Municipalidad y en ese momento estaba tapiada.

Por supuesto que la propiedad recibida no podía ser utilizada de inmediato, pues se encontraba en condiciones
muy desmejoradas, luego de haber sido refugio de habitantes indeseables de todo tipo, *de 2 y 4 patas*, haber
sido depósito de basura de los vecinos de los alrededores, y finalmente tapiada por seguridad.

{{< figure src="/images/historia/casa_1996.jpg" title="Estado inicial en el cual se recibió el edificio de la Escuela" >}}

De todos modos, era una casa nueva para nosotros, así que los coreutas, en lugar de formarse por cuerdas para cantar, se transformaron en
cuadrillas de limpieza, tomaron escobas, palas, hachuelas, martillos, etc., para desechar lo que no servía, y levantaron nuevas paredes y revoques
donde era necesario, para hacer nuestra casa habitable.

<font color="red">Agregar imagen</font>
{{< figure src="/images/historia/salon_actual.jpg" title="Salón actual de la Escuela" >}}

Luego de varios años y algunas renovaciones del Comodato, El Municipio sugirió que en lugar de renovar el Comodato, se pensara en la posibilidad
de lo que se denomina *Donación con cargo*. La Donación con Cargo es la donación de la propiedad por parte del Municipio, condicionada a que
se utilice para el fin especificado, debiendo la Ecco hacerse cargo de los costos de escrituración, e impuestos. Y que, en caso de comprobarse que la
casa no fuera utilizada con el fin previsto, la propiedad volvería a manos del Municipio. Dado que el plazo de Comodato estaba finalizando, se optó
por renovarlo, y con posterioridad encarar el pedido de Donación, era una forma de ser más dueños de nuestra querida sede.

<font color="red">Agregar imagen</font>
{{< figure src="/images/historia/tasca.jpg" title="La Tasca Cultural" >}}

Con fecha 15 de Mayo de 2006, la Escuela es notificada de la promulgación de la *Ordenanza N° 17278*, mediante el *Decreto N° 529* del 7 de Marzo
de 2006, donde consta que la Municipalidad dona a la Institución el inmueble solicitado, ubicado en la calle Catamarca 1065. Y en la reunión de
Comisión Directiva del 4 de Julio de 2006 se procedió a aprobar por unanimidad la aceptación de la donación.

Dado que la Escuela a lo largo de los años ha progresado, lenta pero firmemente, en el espíritu de las autoridades y coreutas continúa la idea de los
que nos legaron esta Institución... Es decir, esta historia continuará y seguramente alguien en un par de años tomará la posta y seguirá escribiendo.

{{< figure src="/images/historia/frente_actual.jpg" title="Frente del edificio de la Escuela" >}}

# Directores históricos

## Coro Estable

{{< directores >}}
{{< figure src="/images/historia/manuel_rego.jpg" title="Manuel Rego">}}
{{< figure src="/images/historia/hector_zeoli.jpg" title="Héctor Zeoli" >}}
{{< figure src="/images/historia/pierre_brannens.jpg" title="Pierre Brannens" >}}
{{< figure src="/images/historia/reynaldo_zemba.jpg" title="Reynaldo Zemba" >}}
{{< figure src="/images/historia/mary_morales.jpg" title="Mary Morales" >}}
{{< figure src="/images/historia/pablo_canaves.jpg" title="Pablo Canaves" >}}
{{< figure src="/images/historia/emir_saul.jpg" title="Emir Saúl" >}}
{{< figure src="/images/historia/victor_meloni.jpg" title="Víctor Meloni" >}}
{{< figure src="/images/historia/marcela_castiglioni.jpg" title="Marcela Castiglioni" >}}
{{< /directores >}}

## Coro Tomá Mate
{{< directores >}}
{{< figure src="/images/historia/cecilia_alvarez.jpg" title="Cecilia Álvarez" >}}
{{< /directores >}}

# Conciertos memorables
* Misa Brevis en Si bemol, de Mozart
* Cantata del café No 211 de J. S. Bach
* Misa en Tiempo de Guerra, de Haydn
* Misa de Réquiem de W. A. Mózart
* Fantasía Coral opus. 80, de Beethoven
* “Romance del Rey Rodrigo”, de Julián Bautista
* Canciones Indianas, de Carlos Guastavino
* La Misa Criolla, con Ariel Ramírez y Los Fronterizos
* Misa Solemnis en Do mayor, de W. A. Mozart
* Oratorio a Carlos Gardel de H. Salgán, U. De Lío y H. Ferrer
* “Macias Enamorado”, de I. Maistegui
* Cantata a San Martín, de Pedro Dondas
* Magnificat, de J. S. Bach
* Te Deum, de Jorge González
* Misa de Réquiem, de Jorge González
* 120° Aniversario de Mar del Plata. Mariano Mores eligió al
    coro de la ECCo para acompañar su concierto en el Playón
    de Las Toscas.