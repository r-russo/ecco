---
title: "Coro Estable"
subtitle: ""
# meta description
description: "This is meta description"
draft: false
---

## Avisos importantes

* Ensayos suspendidos por el momento

## Horario de ensayo

*Martes y jueves de 20 a 22hs*

## Partituras

* [Folklore](#)
* [Tango y milonga](#)
* [Religioso](#)
* [Popular](#)
* [Clásico](#)
* [Villancicos](#)

## Historia
El coro estable nació junto a la institución, en octubre de 1957, si bien la idea primigenia fue fundar una escuela, durante largo tiempo fue lo que mantuvo en funciones a la institución. Como todo grupo humano en reciente formación tuvo sus altibajos, pero la musa Euterpe los mantuvo unidos y progresando. Su historia hasta 1998, en que se crea la segunda actividad dentro de ella, es una con la de la institución misma.

{{< figure src="/images/coro_estable/misa.jpg" title="Ensayo previo a la presentación de la *Misa Criolla* con Los Fronterizos como solistas, Domingo Cura en percusión y la dirección del Maestro Ariel Ramírez">}}

El estreno del Oratorio a Carlos Gardel, de los maestros Horacio Salgán y Ubaldo de Lío, con versos de Horacio Ferrer; obra que fue reeditada en 1997, también junto a sus autores y dirigida por Guillermo Salgán, hijo del autor.La presentación del *Réquiem en Re Menor K.V.626* de Wolfgang Amadeus Mozart, bajo la Dirección de Pierre Brannens acompañado del Conjunto Coral de Cámara de Tandil y la Orquesta Sinfónica Municipal.

{{< figure src="/images/coro_estable/requiem.jpg" title="Presentación del Réquiem de Mozart">}}

En Diciembre de 1972, bajo la dirección de Reynaldo Zemba se presentó en Mar del Plata la *Misa en Tiempo de Guerra* de J. Haydn junto a la orquesta Sinfónica Municipal, en varias oportunidades, con el maestro Ricardo Barrera en la ciudad de Miramar acompañados por el coro Municipal de Miramar; con la Orquesta Sinfónica Municipal, dirigida por el maestro Guillermo Becerra, con solistas del Teatro Colón de Buenos Aires, en el Super Domo de Mar del Plata.

En 1994, junto al maestro Mariano Mores participamos de los festejos del 120 aniversario de la fundación de nuestra ciudad, Mar del Plata.

{{< figure src="/images/coro_estable/magnificat.jpg" title="El 9/9/1991 presentación del Magnificat de J. S. Bach en la Ciudad de Miramar, iglesia San Andres, con motivo del festejo del centenario de Miramar; junto al coro Municipal de Miramar. Participaron solistas locales de la talla de Ana Ruzieska, Edith Villalba, Alejandro Prieto, Claudio Santome y Marcos Devoto, bajo la batuta del maestro Ricardo Barrera">}}

En 1996 realizamos una gira de 15 días por España; ofreciendo conciertos en varias ciudades de Galicia, como Cambados, Lalín, Ponteareas, Villagarcía de Arousa y A Estrada; habiendo participado también en la Misa del Peregrino, en la Catedral de Santiago de Compostela.

{{< figure src="/images/coro_estable/santiago.jpg" title="En las fiestas de Corpus Christi del año 1996 el coro viajo invitado por la Mancomunidad Gallega a cantar a dicha región de España, la foto muestra al Coro en el interior de la catedral de Santiago de Compostela previo a cantar en la misa del peregrino">}}

En 1998 cantamos en la Misa Solemne del 19 de Julio, en la Basílica de San Pedro en el Vaticano, y por la noche ofrecimos un concierto en la iglesia de San Ignacio, en Roma, donde estrenamos para el viejo mundo el TE DEUM del maestro marplatense Jorge González; invitados por la Asociación Internacional de Amigos de la Música Sacra.

{{< figure src="/images/coro_estable/sanpedro.jpg" title="Foto tomada luego de la Misa Solemne del 19/7/98, en la Basilica de San Pedro, Ciudad del Vaticano">}}

La gira incluyó presentaciones en Florencia, París, Burgos y Madrid. Al retorno se hace una gira por la ciudad de Buenos Aires cantando en la Catedral Metropolitana y en la Iglesia Nuestra Señora del Pilar, de Recoleta 	

En 1999 el coro  viajó a Junín invitado por el Coro polifónico Luis Alleva, para presentarse en el encuentro anual que se efectúa en esta hermosa ciudad bonaerense, en la iglesia San Ignacio de Loyola. En el año 2000 comienzan los preparativos para una nueva gira internacional por Galicia, donde llevaríamos la Misa Criolla de Ariel Ramírez

{{< figure src="/images/coro_estable/sanignacio.jpg" title="Foto al finalizar la presentación en la Iglesia de San Ignacio de Roma, invitados por la Asociación Internacional Amigos de la Música Sacra">}}

Contamos en esa oportunidad con la colaboración desinteresada de artistas marplatenses como Pablo Drago en charango, Buchi Pasolini en percusión y Pablo Olmedo en voz solista y guitarra. Durante diciembre de ese año se presentó este espectáculo en diversas oportunidades, siendo su reestreno en el aniversario del coro Municipal de Miramar donde fuimos invitados a participar.
Esta Gira se malogro por problemas con la línea aérea días antes de la partida.

{{< figure src="/images/coro_estable/junin.jpg" title=" Tomando solcito antes de ir a concierto en el cuartel que nos cobijó  - Junín 2/10/1999">}}

Se continua con presentaciones en diversos escenarios y se recupera la moral luego del fracasado tercer viaje, y el 1 de Noviembre de 2003 se estrena luego de un largo tiempo de ensayo la Misa de Réquiem de Jorge  González en la Catedral de los Santos Pedro y Cecilia recibiendo el beneplácito de los asistentes a dicha presentación

Actualmente la dirección del coro está en manos de la profesora Marcela María Castiglioni, con la asistencia de dirección de la profesora Mary Morales tambien encargada del taller de formación coral.

# Galería de fotos

{{< galeria >}}
{{< figure src="/images/fotos/coro_pierre_brannens.jpg" title="" class="slide">}}
{{< figure src="/images/fotos/img7402.jpg" title="" class="slide">}}
{{< figure src="/images/fotos/img7404.jpg" title="" class="slide">}}
{{< figure src="/images/fotos/img8377.jpg" title="" class="slide">}}
{{< figure src="/images/fotos/img8384.jpg" title="" class="slide">}}
{{< figure src="/images/fotos/concierto_canal8.jpg" title="" class="slide">}}
{{< figure src="/images/fotos/magnificat.jpg" title="" class="slide">}}
{{< figure src="/images/fotos/coro_actual.jpg" title="" class="slide">}}
{{< /galeria >}}